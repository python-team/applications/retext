#!/usr/bin/python3

import sys
sys.path.insert(0, '/usr/share/retext')

from ReText.__main__ import main

if __name__ == '__main__':
    main()
